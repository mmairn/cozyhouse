const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin'); // Sve opcije na: https://github.com/jantimon/html-webpack-plugin -> Options. Ima i ostale korisne pluginove.
const CopyPlugin = require('copy-webpack-plugin');
const WriteFileWebpackPlugin = require('write-file-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const path = require('path');
const Terser = require('terser'); // https://www.npmjs.com/package/terser
const webpack = require('webpack');
const TerserPlugin = require('terser-webpack-plugin'); // "mode: 'production'"
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin'); // "mode: 'production'"


module.exports = (env, options) => {
    return {
        devtool: 'inline-source-map',
        devServer: {
            liveReload: false,
            contentBase: './dist',
        },
        optimization: {
            minimizer: [new TerserPlugin({}), new OptimizeCSSAssetsPlugin({})],
        },
        entry: {
            'app': './src/app.ts',
            // 'style': './src/design/style.scss',
        },
        output: {
            filename: '[name].bundle.js',
            path: path.resolve(__dirname, 'dist')
        },
        resolve: {
            extensions: [".ts", ".tsx", ".js", ".jsx", ".scss"],
        },
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    loader: "ts-loader",
                    exclude: /node_modules/
                },
                {
                    test: /\.(s?)css$/,
                    use: [{
                        loader: MiniCssExtractPlugin.loader,

                    },
                        {loader: "css-loader", options: {sourceMap: true}},
                        {
                            loader: "postcss-loader",
                            options: {
                                sourceMap: true,
                                ident: 'postcss',
                                plugins: [
                                    require('autoprefixer')
                                ]
                            }
                        },
                        {loader: "resolve-url-loader"},
                        {loader: "sass-loader", options: {sourceMap: true}},

                    ]
                },
                {
                    test: /(\.png|\.gif|\.ttf|\.eot|\.woff|\.svg)/,
                    loader: "file-loader"
                },
            ],
        },
        plugins: [
            new webpack.ProvidePlugin({
                $: 'jquery',
                jQuery: 'jquery'
            }),
            new HtmlWebpackPlugin({
                filename: 'index.html',
                template: './src/index.html',
                minify: options.mode === "production" ? {
                    collapseWhitespace: true,
                    removeComments: true,
                    removeRedundantAttributes: true,
                    removeScriptTypeAttributes: true,
                    removeStyleLinkTypeAttributes: true,
                    useShortDoctype: true
                } : false,
            }),
            new MiniCssExtractPlugin({
                filename: options.mode === "production" ? '[name].[hash].css' : '[name].css',
                chunkFilename: '[id].css',
                sourceMap: true
            }),
            new CopyPlugin([
                {
                    from: './node_modules/jquery/dist/jquery.min.js',
                    to: './'
                },
                {
                    from: './src/scripts/vendor/uibuilder-1.7.0.js',// Preuzeto sa: https://github.com/wisercoder/uibuilder
                    to: './uibuilder.min.js',
                    transform: function (content, path) {
                        return Terser.minify(content.toString()).code;
                    },
                },
                {
                    from: './src/design/images/favicon.png',
                    to: './'
                },
                {
                    from: './src/design/images/Jennifer.png',
                    to: './images'
                },
                {
                    from: './src/design/images/Katrine.png',
                    to: './images'
                },
                {
                    from: './src/design/images/Woody.png',
                    to: './images'
                },
            ]),
            new WriteFileWebpackPlugin(),
            new CleanWebpackPlugin({
                dry: true
            }),
        ]
    }
};