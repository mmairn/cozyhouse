import "./design/style.scss";
import {Carousel} from "./scripts/Carousel";

const MESSAGE_OF_LOVE = "Come back and adopt L\u2764ve!";

$(document).ready(function () {
    console.log("It's ready!");

    changeTitleContent();
    markActiveLinkOnScroll();

    function changeTitleContent() {

        const isChangeTitleContentAvailable: boolean = true;
        if (!isChangeTitleContentAvailable) {
            return;
        }

        // Ostali simboli se nalaze ovdje: http://www.fileformat.info/info/unicode/char/search.htm
        const message = MESSAGE_OF_LOVE;
        let originalMessage: string = null;

        window.onfocus = function () {
            if (originalMessage) {
                document.title = originalMessage;
            }
        };

        window.onblur = function () {
            const title = document.title;
            if (title !== message) {
                originalMessage = title;
            }
            document.title = message;
        };
    }


    function markActiveLinkOnScroll() {

        const enum classes {
            Header = "ch-header__container",
            HeaderFixed = "ch-header__container--fixed",
            Menu = "ch-menu",
            MenuItemActive = "ch-menu__item--active",
        }

        const durationAnimation = 500;
        const header = <HTMLElement>document.querySelector(`.${classes.Header}`);
        const menu = <HTMLElement>document.querySelector(`.${classes.Menu}`);
        const menuItems = menu.querySelectorAll("a[href]");

        menuItems.forEach((menuItem) => {
            menuItem.addEventListener("click", function (e) {
                e.preventDefault();
                const target = this.getAttribute("href");
                const headerHeight = header.offsetHeight;
                let fixedHeaderHeight = 0;
                let paddingTopToContent = 0;

                if (!isHeaderFixed()) {
                    // Kalkulacija odgovara klasama u header.scss - .ch-header__container--fixed i .ch-header__container--fixed + .ch-content
                    fixedHeaderHeight = 80;
                    paddingTopToContent = 200;

                    const isWindowWidthLessThan: boolean = window.outerWidth <= 740;
                    if (isWindowWidthLessThan) {
                        fixedHeaderHeight = 160;
                        paddingTopToContent = 240;
                    }
                }

                const offsetTop: number = document.getElementById(target.slice(1)).offsetTop - headerHeight + paddingTopToContent - fixedHeaderHeight + 1;

                // Vaniliju mijenjamo čokoladom radi jednostavnosti :-)
                $('html, body').stop().animate({
                    scrollTop: offsetTop
                }, durationAnimation);

            });
        });


        window.onscroll = function () {

            // region Postavljamo fixed klasu
            const sticky = header.offsetTop;
            if (window.pageYOffset > sticky) {
                header.classList.add(classes.HeaderFixed);
            } else {
                header.classList.remove(classes.HeaderFixed);
            }
            // endregion

            const headerOuterHeight = header.offsetHeight;
            const scrollDistance = window.scrollY + headerOuterHeight + 1;

            if (!isHeaderFixed()) {
                menuItems.forEach((menuItem) => {
                    menuItem.parentElement.classList.remove(classes.MenuItemActive);
                });
                return;
            }

            document.querySelectorAll('.ch-content > [id]').forEach(function (item: HTMLElement) {
                const isPositionLessOrEqualThanScrollDistance: boolean = item.offsetTop <= scrollDistance;
                if (isPositionLessOrEqualThanScrollDistance) {
                    const currentMenuItemID = item.getAttribute('id');
                    menuItems.forEach((menuItem) => {
                        menuItem.parentElement.classList.remove(classes.MenuItemActive);
                    });
                    menu.querySelector(`[href="#${currentMenuItemID}"]`).parentElement.classList.add(classes.MenuItemActive);
                }
            });
        };


        function isHeaderFixed(): boolean {
            return document.querySelector(`.${classes.Header}`).classList.contains(classes.HeaderFixed);
        }

    }


    // API, where are you?! :-)
    new Carousel({
        appendAfter: "#pets .ch-pets__title",
        data: [
            {Name: "Katrine 0", Image: "Katrine.png"},
            {Name: "Jennifer 1", Image: "Jennifer.png"},
            {Name: "Woody 2", Image: "Woody.png"},
            {Name: "Petar 3", Image: "Katrine.png"},
            {Name: "Ivan 4", Image: "Woody.png"},
            {Name: "Marko 5", Image: "Jennifer.png"},
            {Name: "Stjepan 6", Image: "Woody.png"}
        ]
    });


});

