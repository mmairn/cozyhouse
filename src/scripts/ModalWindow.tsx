import "../design/components/ModalWindow.scss";

interface IDescriptionData {
    text: string,
    value: string
}

interface IDataItem {
    Image: string, // todo: Inače, da ne slijedimo template, možemo utrpati životinje preko  http://lorempixel.com/470/470/animals/1/
    Name: string,
    Breed: string,
    Story: string,
    Description: IDescriptionData[]
}

interface IModalWindowConfig {
    data: IDataItem
}

const enum classes {
    BodyModalOpen = 'ch-modal__open',
    WindowModalContainerActive = 'ch-window__modal-container--active'
}

const enum elements {
    Body = "body",
    WindowModalContainer = '.ch-window__modal-container',
    WindowContainer = '.ch-window__container',
    WindowCloseButton = '.ch-window__close'
}


export function setImage(imageName: string): string {
    return `background-image: url("./images/${imageName}")`
}


module ModalWindowTemplate {

    export function render(data: IDataItem): HTMLElement {


        const result = (<div class="ch-window__container">
            <div class="ch-window-close__container">
                <div class="ch-button__circle ch-window__close ch-window__close--close"></div>
            </div>
            <div class="ch-window">
                <div class="ch-window__image" style={setImage(data.Image)}></div>
                <div class="ch-window__description">
                    <div class="ch-window__name">{data.Name}</div>
                    <div class="ch-window__breed">{data.Breed}</div>
                    <div class="ch-window__story">{data.Story}</div>
                    {renderDescription(data.Description)}
                </div>
            </div>
        </div>);

        return (<div class="ch-window__modal-container ch-window__modal-container--active">{result}</div>)

    }

    function renderDescription(descriptionData: IDescriptionData[]): HTMLElement {
        const result = descriptionData.map((item) =>
            <div class="ch-window__item">
                <div class="ch-window-item__text">{item.text}:</div>
                <div class="ch-window-item__value">{item.value}</div>
            </div>
        );


        return (<div class="ch-window__list">{result}</div>);

    }

}

export function ModalWindow(config: IModalWindowConfig) {
    if (!(this instanceof ModalWindow)) {
        throw new TypeError("ModalWindow constructor cannot be called as a function!");
    }

    this.config = config;


    const template = ModalWindowTemplate.render(config.data);

    $(elements.Body).append(template);
    $(elements.Body).addClass(classes.BodyModalOpen);

    $(elements.WindowCloseButton).on('click', function () {
        $(elements.WindowModalContainer).remove();
        $(elements.Body).removeClass(classes.BodyModalOpen);
    });


    // Zatvaranje filtera klikom na modalnu podlogu
    $(elements.WindowModalContainer).on('click', function (e) {
        const isModalActive: boolean = $(elements.WindowModalContainer).hasClass(classes.WindowModalContainerActive) === true;
        if (isModalActive) {
            const isWindowContainerTargeted: boolean = $(e.target).closest(elements.WindowContainer).length > 0;
            if (!isWindowContainerTargeted) {
                $(this).removeClass(classes.WindowModalContainerActive);
                $(elements.Body).removeClass(classes.BodyModalOpen);
            }
        }
    });


}


ModalWindow.prototype = {
    constructor: ModalWindow
};