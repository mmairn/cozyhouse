import "../design/components/Carousel.scss";
import {ModalWindow, setImage} from "./ModalWindow";

interface IItem {
    Name: string;
    Image: string; // todo: LoremPixel
}

interface ICarouselConfig {
    appendAfter: string,
    data: IItem[]
}

const enum elements {
    ButtonCarouselLeft = '.ch-carousel__arrow--left',
    ButtonCarouselRight = '.ch-carousel__arrow--right',
    CarouselItem = '[data-item-order]',
    CarouselItemFirst = '[data-item-order]:first',
    CarouselItemLast = '[data-item-order]:last',
    CarouselItemVisible = '[data-item-order]:visible',
    CarouselItemVisibleFirst = '[data-item-order]:visible:first',
    CarouselItemVisibleLast = '[data-item-order]:visible:last',
    CarouselItemMore = '.ch-carousel-item__more',
}

const enum attributes {
    Order = "data-item-order",
    Name = "data-item-name",
    Image = "data-item-image"
}

module CarouselTemplate {

    export function render(data: IItem[]): HTMLElement {

        const result = (<div class="ch-carousel">
            <div class="ch-button__circle ch-carousel__arrow ch-carousel__arrow--left"></div>
            {renderItems(data)}
            <div class="ch-button__circle ch-carousel__arrow ch-carousel__arrow--right"></div>
        </div>);


        return result;
    }


    function renderItems(data: IItem[]): HTMLElement {

        let count = 0;
        const result = data.map((item: IItem) =>
            <div class="ch-carousel__item" data-item-order={count++} data-item-name={item.Name}
                 data-item-image={item.Image}>
                <div class="ch-carousel-item__image" style={setImage(item.Image)}></div>
                <div class="ch-carousel-item__title">{item.Name}</div>
                <div class="ch-carousel-item__more">Learn More</div>
            </div>);


        return (
            <div class="ch-carousel__container">
                {result}
            </div>
        )

    }

}

export function Carousel(config: ICarouselConfig) {
    if (!(this instanceof Carousel)) {
        throw new TypeError("Carousel constructor cannot be called as a function!");
    }


    // Ako zatreba, spread umjesto _.defaults ;-)
    // const _config = {
    //     a: "b",
    //     ...config
    // };

    this.config = config;


    const template = CarouselTemplate.render(config.data);
    $(config.appendAfter).after(template);


    const step = $(elements.CarouselItemVisible).length;
    const items = $(elements.CarouselItem);

    $(elements.ButtonCarouselLeft).on('click', function () {

        let currentOrder = parseInt($(elements.CarouselItemVisibleFirst).attr(attributes.Order));

        const isFirstItemShown: boolean = $(elements.CarouselItemFirst).is(':visible');
        if (isFirstItemShown) {
            currentOrder = items.length;
        }
        $(elements.CarouselItemVisible).css("display", "none");
        let count = 0;
        while (count < step) {
            currentOrder--;
            $(`[${attributes.Order}=${currentOrder}]`).css("display", "flex");
            count++;
        }

        const numberOfVisibleItems = $(elements.CarouselItemVisible).length;
        const isNumberOfVisibleItemsLessThanStepNumber: boolean = numberOfVisibleItems < step;
        if (isNumberOfVisibleItemsLessThanStepNumber) {
            $(elements.CarouselItemVisible).css("display", "none");
            let tempOrder = -1;
            const tempStep = step - 1;
            while (tempOrder < tempStep) {
                tempOrder++;
                $(`[${attributes.Order}=${tempOrder}]`).css("display", "flex");
            }
        }
    });


    $(elements.ButtonCarouselRight).on('click', function () {

        let currentOrder = parseInt($(elements.CarouselItemVisibleLast).attr(attributes.Order));

        const isLastItemShown: boolean = $(elements.CarouselItemLast).is(':visible');
        if (isLastItemShown) {
            currentOrder = -1;
        }
        $(elements.CarouselItemVisible).css("display", "none");
        let count = 0;
        while (count < step) {
            currentOrder++;
            $(`[${attributes.Order}=${currentOrder}]`).css("display", "flex");
            count++;
        }

        const numberOfVisibleItems = $(elements.CarouselItemVisible).length;
        const isNumberOfVisibleItemsLessThanStepNumber: boolean = numberOfVisibleItems < step;
        if (isNumberOfVisibleItemsLessThanStepNumber) {
            $(elements.CarouselItemVisible).css("display", "none");
            let tempOrder = currentOrder;
            const tempStep = items.length - step;
            while (tempOrder > tempStep) {
                tempOrder--;
                $(`[${attributes.Order}=${tempOrder}]`).css("display", "flex");
            }
        }

    });

    // todo: Uobičajeno ovdje ide neki poziv API-ju (.closest attr id za dohvat ID-a)
    $(elements.CarouselItemMore).on('click', function (e) {
        const target = $(e.target).closest(elements.CarouselItemVisible);

        const itemName = target.attr(attributes.Name);
        const itemImage = target.attr(attributes.Image);


        // Where is my API calls? :-)
        new ModalWindow({
            data: {
                Image: itemImage,
                Name: itemName,
                Breed: "Pooch",
                Story: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce efficitur blandit condimentum. Proin accumsan lorem vel gra vida fringilla. Suspendisse potenti. Mauris ut pulvinar nunc. Donec consectetur, diam in porta tempus, urna ligula ves tibulum nibh.",
                Description: [
                    {text: "Age", value: "4 months"},
                    {text: "Inoculations", value: "none"},
                    {text: "Diseases", value: "none"},
                    {text: "Parasites", value: "none"}
                ]
            }
        });


    });
}

Carousel.prototype = {
    constructor: Carousel
};